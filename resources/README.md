These are Cordova resources. You can replace icon.png and splash.png and run
`ionic cordova resources` to generate custom icons and splash screens for your
app. See `ionic cordova resources --help` for details.

Cordova reference documentation:

- Icons: https://cordova.apache.org/docs/en/latest/config_ref/images.html
- Splash Screens: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-splashscreen/


export ANDROID_HOME=/Users/apple/Library/Android/sdk
export ANDROID_SDK_ROOT=/Users/apple/Library/Android/sdk
export ANDROID_AVD_HOME=/Users/apple/.android/avd
