import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-mytier',
  templateUrl: './mytier.page.html',
  styleUrls: ['./mytier.page.scss'],
})
export class MytierPage implements OnInit {
  userObj : any;
  constructor(private sessionService : SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
  }

}
