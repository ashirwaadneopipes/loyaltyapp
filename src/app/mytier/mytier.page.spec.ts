import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytierPage } from './mytier.page';

describe('MytierPage', () => {
  let component: MytierPage;
  let fixture: ComponentFixture<MytierPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytierPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytierPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
