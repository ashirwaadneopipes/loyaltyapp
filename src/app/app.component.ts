import { Component, ViewChildren, QueryList } from '@angular/core';

import { Platform, MenuController, ModalController, PopoverController, ToastController, IonRouterOutlet, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SessionService } from './services/session.service';
import { GeneralPurposeService } from './services/general-purpose.service';
import { ApiCallsService } from './services/api-calls.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Network } from '@ionic-native/network/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';




@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  // set up hardware back button event.
  imgUrlDomain = environment.baseurl + '/uploads/user/profile/';
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  $userObj: any;
  userObj: {};
  blah: boolean = true;

  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  unsyncedSKUs: any;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menuCtrl: MenuController,
    private sessionService: SessionService,
    private genPurpose: GeneralPurposeService,
    private apiService: ApiCallsService,
    private router: Router,
    public modalCtrl: ModalController,
    private menu: MenuController,
    private navCtrl: NavController,
    private popoverCtrl: PopoverController,
    private toast: ToastController,
    private network: Network,
    private geolocation: Geolocation,

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      // this.router.navigateByUrl('/dashboard');
      this.statusBar.styleDefault();
      // this.statusBar.backgroundColorByHexString('#ffffff');
      if (localStorage.getItem('loggedIn') != null) {
        this.router.navigateByUrl('/dashboard');
      }

      this.$userObj = this.sessionService.userObjObservable.subscribe((data) => {
        this.userObj = data;
      });
      // let connectSubscription = this.network.onConnect().subscribe(() => {
      //   console.log('network connected!');
      //   if(this.getAllUnSyncedSKU()) {
      //     this.getLatLong();
      //     this.genPurpose.showToast(`Your unsynced SKU's will be uploaded`,'top','dark');
      //   }
      // });
    });
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  logoutUser() {
    this.sessionService.clearSession();
    this.navCtrl.navigateRoot('/enter-mobile');
    // this.router.navigateByUrl('enter-mobile');
  }

  gotoPortfolio() {
    this.menuCtrl.close();
    this.router.navigate(['/portfolio']);
  }

  getLatLong() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.uploadSKU(resp.coords.latitude, resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  uploadSKU(lat, long) {
    const payload = {
      qr_codes: this.getQRCodes(),
      coordinate: {
        latitude: lat,
        longitude: long
      }
    };
    console.log(payload);
    this.apiService.uploadSKU(payload).toPromise().then((resp: any) => {
      console.log(resp);
      this.genPurpose.showToast('Unsynced SKU uploaded', 'top', 'dark');
      localStorage.removeItem('unsyncedSKU');
    },
      (err: any) => {
        this.genPurpose.showToast(this.genPurpose.commonText.SERVICE_ERROR, 'top', 'dark');
      });
  }

  getQRCodes() {
    const qrCodeArr = [];
    for (let item of this.unsyncedSKUs) {
      qrCodeArr.push({
        QR: item.qrCode
      });
    }
    return qrCodeArr;
  }

  getAllUnSyncedSKU() {
    const unsyncSKU = localStorage.getItem('unsyncedSKU');
    if (unsyncSKU != null) {
      this.unsyncedSKUs = JSON.parse(unsyncSKU);
      console.log('unsynced SKUs', this.unsyncedSKUs);
      return true;
    };
  }

}
