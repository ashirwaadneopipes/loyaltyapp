import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { GeneralPurposeService } from 'src/app/services/general-purpose.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.page.html',
  styleUrls: ['./question-detail.page.scss'],
})
export class QuestionDetailPage implements OnInit {
  questionId;
  questionDetail: any = undefined;
  myReply: string = '';
  userObj = ''

  constructor(private route: ActivatedRoute, private apiService: ApiCallsService, private genPurpose: GeneralPurposeService,
    private sessionService: SessionService) { }

  ngOnInit() {
    this.questionId = this.route.snapshot.paramMap.get('id');
    this.getQuestionById();
    this.getUserDP();
  }

  getUserDP() {
    this.userObj = this.sessionService.getUserDetails();
    //console.log(this.userObj);
  }

  getQuestionById() {
    const payload = {
      _id: this.questionId
    };
    this.genPurpose.setLoaderStatus(true);
    this.apiService.fetchQuestionDetails(payload).toPromise().then((resp: any) => {
      console.log(resp);
      this.questionDetail = resp.data;
      this.genPurpose.setLoaderStatus(false);
    },
      (err: any) => {
        this.genPurpose.setLoaderStatus(false);
        this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
      });
  }

  formatDate(date) {
    return moment(date).format('DD MMM YYYY');
  }

  getImageFullPath(imgName) {
    return `${environment.baseurl}/uploads/user/profile/${imgName}`;
  }

  getAttachmentPath(imgName) {
    return `${environment.baseurl}/uploads/question/${imgName}`;
  }

  postAComment() {
    if (this.myReply.trim() == '') {
      this.genPurpose.showToast('Please write something before pressing the send button', 'top', 'dark');
    }
    else {
      const payload = {
        comment: this.myReply,
        question: this.questionId
      };

      //this.genPurpose.setLoaderStatus(true);
      this.apiService.postComment(payload).toPromise().then((resp: any) => {
        console.log(resp);
        // this.questionDetail = resp.data;
        this.myReply = '';
        this.getQuestionById();
        this.genPurpose.setLoaderStatus(false);
      },
        (err: any) => {
          this.genPurpose.setLoaderStatus(false);
          this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
        });
    }
  }

}
