import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { GeneralPurposeService } from 'src/app/services/general-purpose.service';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.page.html',
  styleUrls: ['./create-question.page.scss'],
})
export class CreateQuestionPage implements OnInit {
  regions = [{ _id: 1, name: 'Bangalore' }, { _id: 2, name: 'Mumbai' }];
  languages = [{ _id: 1, name: 'Kannada' }, { _id: 2, name: 'English' }];
  categories = [{ _id: 1, name: 'Pipes' }, { _id: 2, name: 'Fittings' }];

  selectedRegion: any;
  selectedLanguage: any;
  selectedCategory: any;
  myQuestion: any = '';
  myDescription: any = '';

  viewDocumentPath;
  documentPath = '';
  constructor(private actionCtrl: ActionSheetController, private camera: Camera, private sanitizer: DomSanitizer,
    private genPurpose: GeneralPurposeService, private apiService: ApiCallsService, private router: Router) { }

  ngOnInit() {
  }

  createQuestion() {
    console.log({
      region: this.selectedRegion,
      language: this.selectedLanguage,
      category: this.selectedCategory,
      question: this.myQuestion,
      desc: this.myDescription
    });

    if (this.selectedRegion == undefined || this.selectedLanguage == undefined || this.selectedCategory == undefined || this.myQuestion.trim() == '' || this.myDescription.trim() == '') {
      this.genPurpose.showToast(`'*' marked fields are mandatory!`, 'top', 'dark');
    }
    else {
      this.genPurpose.setLoaderStatus(true);
      const payload = {
        question: this.myQuestion,
        description: this.myDescription,
        language: this.selectedLanguage,
        region: this.selectedRegion,
        category: this.selectedCategory,
        question_image: this.documentPath
      };
      this.apiService.postNewQuestion(payload).toPromise().then((resp: any) => {
        this.genPurpose.setLoaderStatus(false);
        console.log(resp);
        this.router.navigateByUrl('/community');
      },
        (err: any) => {
          this.genPurpose.setLoaderStatus(false);
          this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
        });
    }
  }

  promptMediaOption() {
    this.actionCtrl.create({
      buttons: [{
        text: 'Camera',
        icon: 'ios-camera',
        handler: () => {
          const options: CameraOptions = {
            quality: 10,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.CAMERA,
            // allowEdit: true
          };
          this.getPicture(options);
        }
      }, {
        text: 'Gallery',
        icon: 'md-photos',
        handler: () => {
          const options: CameraOptions = {
            quality: 10,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            // allowEdit: true
          };
          this.getPicture(options);
        }
      }, {
        text: 'Cancel',
        icon: 'md-close',
        role: 'cancel'
      }]
    }).then(actionSheet => {
      actionSheet.present();
    });
  }

  getPicture(options) {
    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      this.viewDocumentPath = this.sanitizeURL(imageData);
      this.documentPath = `data:Image/*;base64, ${imageData}`;
    }, (err) => {
      console.log(err);
    });
  }

  sanitizeURL(imageData) {
    return this.sanitizer.bypassSecurityTrustUrl('data:Image/*;base64,' + imageData);
  }

}
