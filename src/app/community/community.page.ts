import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallsService } from '../services/api-calls.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import * as _ from 'lodash';

@Component({
  selector: 'app-community',
  templateUrl: './community.page.html',
  styleUrls: ['./community.page.scss'],
})
export class CommunityPage implements OnInit {
  emptyList: boolean;
  regions = [{ _id: "1", name: 'Bangalore' }, { _id: "2", name: 'Mumbai' }];
  languages = [{ _id: "1", name: 'Kannada' }, { _id: "2", name: 'English' }];
  categories = [{ _id: "1", name: 'Pipes' }, { _id: "2", name: 'Fittings' }];

  selectedRegion: any;
  selectedLanguage: any;
  selectedCategory: any;

  questionSet: any = [];
  filteredSet: any = [];
  filterObj = {}

  constructor(private router: Router, private apiService: ApiCallsService, private genPurpose: GeneralPurposeService) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.listQuestions();
  }

  listQuestions() {
    this.genPurpose.setLoaderStatus(true);
    this.apiService.listCommunityQuestions().toPromise().then((resp: any) => {
      console.log(resp);
      if (resp.data.length > 0) {
        this.questionSet = resp.data;
        this.emptyList = false;
        this.filterFunction();
      }
      else {
        this.emptyList = true;
      }
      this.genPurpose.setLoaderStatus(false);
    },
      err => {
        this.genPurpose.setLoaderStatus(false);
        this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
      });
  }

  cardClicked(id: any) {
    this.router.navigate(['/question-detail', id]);
  }

  addQuestion() {
    this.router.navigate(['/create-question']);
  }

  formatDate(date) {
    return moment(date).format('DD MMM YYYY');
  }

  getImageFullPath(imgName) {
    return `${environment.baseurl}/uploads/user/profile/${imgName}`;
  }


  filterFunction(){
    console.log(this.filterObj);
    console.log(this.questionSet);
    console.log(_.filter(this.questionSet, this.filterObj));
    // return _.filter(this.questionSet, this.filterObj);
    this.filteredSet = _.filter(this.questionSet, this.filterObj);
  }

  filterSelect(key,event){
    // console.log(event);
    if(key == 'regions'){
      this.filterObj['region'] = this.selectedRegion.toString();
    } else if (key == 'languages') {
      this.filterObj['language'] = this.selectedLanguage.toString();
    } else if (key == 'categories') {
      this.filterObj['category'] = this.selectedCategory.toString();
    }
    this.filterFunction();

  }

}
