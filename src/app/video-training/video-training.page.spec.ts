import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoTrainingPage } from './video-training.page';

describe('VideoTrainingPage', () => {
  let component: VideoTrainingPage;
  let fixture: ComponentFixture<VideoTrainingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoTrainingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoTrainingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
