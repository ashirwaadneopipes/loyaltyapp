import { Component, OnInit } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.page.html',
  styleUrls: ['./video-detail.page.scss'],
})
export class VideoDetailPage implements OnInit {

  embedLink:any = null;
  embedId : any = null;
  constructor(private route :  ActivatedRoute, private router:Router, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.embedId = this.route.snapshot.paramMap.get('id');
    this.getVideoContent();
  }

  getVideoContent() {
    this.embedLink =  this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/'+ this.embedId);
  }
}
