import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-video-training',
  templateUrl: './video-training.page.html',
  styleUrls: ['./video-training.page.scss'],
})
export class VideoTrainingPage implements OnInit {

  
  videoLinksObject = [
    {
      title : 'SWR Installation',
      by : 'Ashirwad Pipes',
      link : 'az2WKCcIyO8'
    },
    {
      title : 'UG Installation',
      by : 'Ashirwad Pipes',
      link : '1vPc7flmhwA',
    },
    {
      title : 'CPVC installation',
      by : 'Ashirwad Pipes',
      link : '4k73crCWpzo',
    },
    {
      title : 'Plumbers Video',
      by : 'Ashirwad Pipes',
      link : '9Jendr34wRs'
    },
    {
      title : 'Walkthrough',
      by : 'Ashirwad Pipes',
      link : '9ueoJjfhHk4'
    },
    {
      title : 'Introduction',
      by : 'Ashirwad Pipes',
      link : '1QmcPCEQGJA'
    },
  ]
  constructor(private router:Router) { }

  ngOnInit() {
  }

  openVideoPage(embedId){
    this.router.navigate(['/video-detail',embedId]);
  }

}
