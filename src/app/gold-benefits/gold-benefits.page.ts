import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-gold-benefits',
  templateUrl: './gold-benefits.page.html',
  styleUrls: ['./gold-benefits.page.scss'],
})
export class GoldBenefitsPage implements OnInit {

  userObj : any;
  constructor(private sessionService : SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
  }

}
