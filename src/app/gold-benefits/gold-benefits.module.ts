import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GoldBenefitsPage } from './gold-benefits.page';

const routes: Routes = [
  {
    path: '',
    component: GoldBenefitsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GoldBenefitsPage]
})
export class GoldBenefitsPageModule {}
