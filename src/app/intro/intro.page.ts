import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  buttonText:string="Skip"
  @ViewChild('Slides') slides: IonSlides;
  constructor(private router: Router) { }

  ngOnInit() {
    const isIntroShown = localStorage.getItem('isIntroShown');
    if (isIntroShown !== null) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  slideChanged(){
    this.slides.isEnd().then(data=>{
      data?this.buttonText='Finish':this.buttonText='Skip'
    })
  }

  skipIntro() {
    this.router.navigateByUrl('/enter-mobile');
  }

}
