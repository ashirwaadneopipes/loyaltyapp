import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RedeemRewardsPage } from './redeem-rewards.page';

const routes: Routes = [
  {
    path: '',
    component: RedeemRewardsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RedeemRewardsPage]
})
export class RedeemRewardsPageModule {}
