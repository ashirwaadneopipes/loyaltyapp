import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { AlertController } from '@ionic/angular';
import { DatatransferService } from '../services/datatransfer.service';

@Component({
  selector: 'app-redeem-rewards',
  templateUrl: './redeem-rewards.page.html',
  styleUrls: ['./redeem-rewards.page.scss'],
})
export class RedeemRewardsPage implements OnInit {
  redeemitems: any[] = [
    {
      "id":1,"heading": "HDFC Gift Voucher","points" : "120","imageUrl":"assets/imgs/HDFC Gift Voucher.png"
    },
    {
      "id":2,"heading": "Ashirvad Stores","points" : "80","imageUrl":"assets/imgs/ashirvad store.png"
    },
    {
      "id":3,"heading": "Flipkart Gift Voucher","points" : "150","imageUrl":"assets/imgs/Flipkart-Logo.png"
    },
    {
      "id":4,"heading": "D-mart Voucher","points" : "50","imageUrl":"assets/imgs/Dmart.png"
    },
    {
      "id":5,"heading": "HDFC Gift Voucher","points" : "100","imageUrl":"assets/imgs/HDFC Gift Voucher.png"
    },
    {
      "id":6,"heading": "Ashirvad Stores","points" : "120","imageUrl":"assets/imgs/ashirvad store.png"
    }
  ];

  constructor(private  alertCtrl : AlertController, private dataService : DatatransferService) { }

  ngOnInit() {
  }

  addToCart(id){
    let cartItems = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    let currentCartPoints = 0;
    // console.log(_.find(cartItems,{'id':id}));
    if (!_.find(cartItems,{'id':id})) {
      let objToAdd = _.find(this.redeemitems,{id : id});
      _.forEach(cartItems, function(value, key) {
        currentCartPoints = currentCartPoints + parseInt(value.points);
      });
      console.log(this.dataService.userObject);
      if(currentCartPoints + parseInt(objToAdd.points) > this.dataService.userObject.current_points){
        this.alertCtrl.create({
          header : 'Points Exceeded!!',
          message : 'Current Product exceeds the redeemable points',
          buttons : ['OK']
        }).then(alert=>{
          alert.present();
        });
        return false;
      }
      cartItems.push(_.find(this.redeemitems,{id : id}));
      this.alertCtrl.create({
        header : 'Added!!',
        message : 'Product Added in cart.',
        buttons : ['OK']
      }).then(alert=>{
        alert.present();
      });
    }
    else {
      this.alertCtrl.create({
        header : 'Already Added!!',
        message : 'Product Already exists in the cart',
        buttons : ['OK']
      }).then(alert=>{
        alert.present();
      });
    }

    // console.log(cartItems);
    localStorage.setItem('cart',JSON.stringify(cartItems));
    // console.log(localStorage.getItem('cart'));
  }

}
