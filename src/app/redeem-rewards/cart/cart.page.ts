import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { GeneralPurposeService } from 'src/app/services/general-purpose.service';
import { AlertController } from '@ionic/angular';
import * as _ from "lodash";
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss']
})
export class CartPage implements OnInit {
  userObj: any;

  constructor(private route : Router, private apiService : ApiCallsService, private genSerive : GeneralPurposeService, private alertCtrl : AlertController, private sessionService : SessionService) { }

  cartItems : any = [];
  ngOnInit() {
    // setTimeout(()=>this.buyNow(),3000);
    // this.cartItems = [
    //   {
    //     heading : 'HDFC Gift Voucher',
    //     imageUrl : '../../assets/svgs/Scan QR Code.svg',
    //     points : 100,
    //   }
    // ]
    this.userObj = this.sessionService.getUserDetails();
    this.cartItems = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
  }

  buyNow(){
    let cartItems = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    let payload = {
      "coupon_codes": _.map(cartItems,(value, index)=>{
        return {'coupon_code' : value.id, 'redeem_points' : value.points}
      })
    };
    console.log(payload);
    this.apiService.buyNow(payload).toPromise().then((data:any)=>{
      console.log(data);
      localStorage.removeItem('cart');
      this.alertCtrl.create({
        header : 'Successful!!',
        message : 'Cart Items Redeemed.',
        buttons : [{
          text : 'OK',
          handler : ()=>{
            this.route.navigate(['/dashboard']);
          }
        }]
      }).then(alert=>{
        alert.present();
      });
    },(err)=>{
      console.log(err);
    });
  }

}
