import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import { ApiCallsService } from '../services/api-calls.service';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-intermediate',
  templateUrl: './intermediate.page.html',
  styleUrls: ['./intermediate.page.scss'],
})
export class IntermediatePage implements OnInit {

  constructor(private sessionService : SessionService, private genService : GeneralPurposeService, private apiService : ApiCallsService, private router : Router,private splashscreen : SplashScreen, private platform : Platform) { }

  ngOnInit() {
    // this.platform.ready().then(() => {
      let userId = this.sessionService.getUserId();
      console.log(userId);
      if (userId) {
        this.genService.setLoaderStatus(true);
        this.apiService.dashboardGetData({'_id':userId}).toPromise().then(
          (data: any) => {
            console.log(data);
            if(data.success){
              this.sessionService.setUserDetails(data.data);
              this.router.navigateByUrl('/dashboard');
            } else {
              this.router.navigateByUrl('/enter-mobile')
            }
            this.genService.setLoaderStatus(false);
          },
          (err: any) => {
            console.log(err);
            this.genService.setLoaderStatus(false);
          }).finally(()=>{
            this.splashscreen.hide();
            this.genService.setLoaderStatus(false);
          });
      } else {
        this.splashscreen.hide();
        this.router.navigateByUrl('/enter-mobile');
      }
    // });
  }

}
