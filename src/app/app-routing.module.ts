import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'enter-mobile', pathMatch: 'full' },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'enter-mobile', loadChildren: './enter-mobile/enter-mobile.module#EnterMobilePageModule' },
  { path: 'enter-otp', loadChildren: './enter-otp/enter-otp.module#EnterOTPPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  { path: 'ash-rewards', loadChildren: './ash-rewards/ash-rewards.module#AshRewardsPageModule' },
  { path: 'community', loadChildren: './community/community.module#CommunityPageModule' },
  { path: 'mytier', loadChildren: './mytier/mytier.module#MytierPageModule' },
  { path: 'gold-benefits', loadChildren: './gold-benefits/gold-benefits.module#GoldBenefitsPageModule' },
  { path: 'question-detail/:id', loadChildren: './community/question-detail/question-detail.module#QuestionDetailPageModule' },
  { path: 'create-question', loadChildren: './community/create-question/create-question.module#CreateQuestionPageModule' },
  { path: 'scan-qr', loadChildren: './ash-rewards/scan-qr/scan-qr.module#ScanQRPageModule' },
  { path: 'unsynced-sku', loadChildren: './ash-rewards/scan-qr/unsynced-sku/unsynced-sku.module#UnsyncedSKUPageModule' },
  { path: 'silver-benefits', loadChildren: './silver-benefits/silver-benefits.module#SilverBenefitsPageModule' },
  { path: 'bronze-benefits', loadChildren: './bronze-benefits/bronze-benefits.module#BronzeBenefitsPageModule' },
  // { path: 'video-training', loadChildren: './video-training/video-training.module#VideoTrainingPageModule' },
  { path: 'upload-summary', loadChildren: './ash-rewards/scan-qr/unsynced-sku/upload-summary/upload-summary.module#UploadSummaryPageModule' },
  { path: 'video-training', loadChildren: './video-training/video-training.module#VideoTrainingPageModule' },
  { path: 'contact-us', loadChildren: './contact-us/contact-us.module#ContactUsPageModule' },
  { path: 'contact-form', loadChildren: './contact-form/contact-form.module#ContactFormPageModule' },
  { path: 'video-detail/:id', loadChildren: './video-training/video-detail/video-detail.module#VideoDetailPageModule' },
  { path: 'catalogue', loadChildren: './catalogue/catalogue.module#CataloguePageModule' },
  { path: 'contact-us', loadChildren: './contact-us/contact-us.module#ContactUsPageModule' },
  { path: 'contact-form', loadChildren: './contact-form/contact-form.module#ContactFormPageModule' },
  { path: 'pdf-view/:id', loadChildren: './catalogue/pdf-view/pdf-view.module#PdfViewPageModule' },
  { path: 'portfolio', loadChildren: './portfolio/portfolio.module#PortfolioPageModule' },
  { path: 'redeem-rewards', loadChildren: './redeem-rewards/redeem-rewards.module#RedeemRewardsPageModule' },
  { path: 'order-status', loadChildren: './order-status/order-status.module#OrderStatusPageModule' },
  { path: 'cart', loadChildren: './redeem-rewards/cart/cart.module#CartPageModule' },
  { path: 'point-balance', loadChildren: './point-balance/point-balance.module#PointBalancePageModule' },
  { path: 'intermediate', loadChildren: './intermediate/intermediate.module#IntermediatePageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
