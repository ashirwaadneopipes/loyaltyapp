import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-ash-rewards',
  templateUrl: './ash-rewards.page.html',
  styleUrls: ['./ash-rewards.page.scss'],
})
export class AshRewardsPage implements OnInit {

  userObj : any;
  constructor(private sessionService : SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
  }


}
