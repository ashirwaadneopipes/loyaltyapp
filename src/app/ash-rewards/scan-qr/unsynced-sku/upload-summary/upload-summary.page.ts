import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DatatransferService } from 'src/app/services/datatransfer.service';

@Component({
  selector: 'app-upload-summary',
  templateUrl: './upload-summary.page.html',
  styleUrls: ['./upload-summary.page.scss'],
})
export class UploadSummaryPage {
  total_Points;
  validSKU;
  qrCodesResult = [];
  serverResponse: any;
  constructor(private router: Router, private dataTransService: DatatransferService) { }

  ionViewWillEnter() {
    this.serverResponse = this.dataTransService.scannedServiceObj;
    console.log(this.serverResponse);
    if (this.serverResponse) {
      this.qrCodesResult = this.serverResponse.data.qr_codes;
      this.total_Points = this.serverResponse.data.total_reward;
      this.validSKU = this.serverResponse.data.valid_qr
    }

  }

  clearSuccessfulonesAndRedirect() {
    const unsuccessfullScans = [];
    for (let item of this.serverResponse.data.qr_codes) {
      if (item.is_dublicates == true || item.Stats == '0') {
        unsuccessfullScans.push(item.QR);
      }
    };
    console.log(unsuccessfullScans);
    localStorage.setItem('unsyncedSKU', unsuccessfullScans.toString());
    this.router.navigateByUrl('/dashboard');
  }

  clearStorage() {
    localStorage.removeItem('unsyncedSKU');
    this.router.navigateByUrl('/dashboard');
  }

}
