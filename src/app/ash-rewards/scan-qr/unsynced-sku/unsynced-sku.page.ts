import { Component } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GeneralPurposeService } from 'src/app/services/general-purpose.service';
import { ApiCallsService } from 'src/app/services/api-calls.service';
import { Router } from '@angular/router';
import { DatatransferService } from 'src/app/services/datatransfer.service';
import * as moment from 'moment';

@Component({
  selector: 'app-unsynced-sku',
  templateUrl: './unsynced-sku.page.html',
  styleUrls: ['./unsynced-sku.page.scss'],
})
export class UnsyncedSKUPage {
  emptyUploads: boolean = false;
  unsyncedSKUs = [];
  constructor(private geolocation: Geolocation, private genPurpose: GeneralPurposeService,
    private apiService: ApiCallsService, private router: Router, private dataTransferService: DatatransferService) { }

  ionViewWillEnter() {
    this.getAllUnSyncedSKU();
  }

  getAllUnSyncedSKU() {
    const unsyncSKU = localStorage.getItem('unsyncedSKU');
    if (unsyncSKU != null) {
      this.unsyncedSKUs = JSON.parse(unsyncSKU);
      console.log('unsynced SKUs',this.unsyncedSKUs);
    } else {
      this.emptyUploads = true;
    }
  }

  convertArrayToObjectArr() {
    for (let arrItem of this.unsyncedSKUs) {
      arrItem.checked = false;
    }
  }

  selectAll() {
    for (let arrItem of this.unsyncedSKUs) {
      arrItem.checked = true;
    }
  }

  selectOrUnselectThisSKU(index) {
    this.unsyncedSKUs[index].checked = !this.unsyncedSKUs[index].checked;
  }

  getLatLong() {
    this.genPurpose.setLoaderStatus(true);
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      this.uploadSKU(resp.coords.latitude, resp.coords.longitude);
      // console.log(resp);
    }).catch((error) => {
      this.genPurpose.setLoaderStatus(false);
      console.log('Error getting location', error);
    });
    // this.router.navigateByUrl('/upload-summary');
  }

  uploadSKU(lat, long) {
    const payload = {
      qr_codes: this.getQRCodes(),
      coordinate: {
        latitude: lat,
        longitude: long
      }
    };
    console.log(payload);
    this.apiService.uploadSKU(payload).toPromise().then((resp: any) => {
      console.log(resp);
      this.genPurpose.setLoaderStatus(false);
      this.dataTransferService.scannedServiceObj = resp;
      this.router.navigateByUrl('/upload-summary');
    },
      (err: any) => {
        this.genPurpose.setLoaderStatus(false);
        this.genPurpose.showToast(this.genPurpose.commonText.SERVICE_ERROR, 'top', 'dark');
      });
  }

  getQRCodes() {
    const qrCodeArr = [];
    for (let item of this.unsyncedSKUs) {
      qrCodeArr.push({
        QR: item.qrCode
      });
    }
    return qrCodeArr;
  }

  checkifAnyChecked() {
    let allUnchecked: boolean = true;
    for (let item of this.unsyncedSKUs) {
      if (item.checked) {
        allUnchecked = false;
      }
    }
    return allUnchecked;
  }

}
