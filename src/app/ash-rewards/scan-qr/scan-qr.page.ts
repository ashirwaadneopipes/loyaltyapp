import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Utility } from '../../validators/utility';
import { GeneralPurposeService } from '../../services/general-purpose.service';
import { ApiCallsService } from '../../services/api-calls.service';
import * as moment from 'moment';

@Component({
  selector: 'app-scan-qr',
  templateUrl: './scan-qr.page.html',
  styleUrls: ['./scan-qr.page.scss'],
})
export class ScanQRPage implements OnInit {
  scannedText = [];
  qrCode: string = "";
  constructor(private barcodeScanner: BarcodeScanner, private platform: Platform, private navCtrl: NavController,
    private router: Router, private genPurpose: GeneralPurposeService, private apiService: ApiCallsService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
  }

  scanQRCode() {
    this.platform.ready().then(() => {
      if (this.platform.is('android') || this.platform.is('ios')) {
        this.barcodeScanner.scan({
          showFlipCameraButton: false, // iOS and Android
          showTorchButton: false, // iOS and Android
          resultDisplayDuration: 0,
          disableAnimations: true,
          disableSuccessBeep: true
        }).then(barcodeData => {
          console.log('Barcode data', barcodeData);
          const qrCode = barcodeData.text;
          if (this.checkForSameEntry(qrCode) === false && qrCode.trim().length === 22 && qrCode.toString().match(Utility.MOBILE_REGX)) {
            this.hitScannedURL(barcodeData.text);
          } else {
            if (!barcodeData.cancelled) {
              if (this.checkForSameEntry(qrCode) === true) {
                this.alertCtrl.create({
                  header: 'Alert!',
                  message: 'Item is already scanned!',
                  buttons: [
                    {
                      text: 'Scan Again',
                      handler: () => {
                        this.scanQRCode();
                      }
                    },
                    {
                      text: 'Done',
                      handler: () => {
                        this.navCtrl.setDirection('forward');
                        this.router.navigateByUrl('/unsynced-sku');
                      }
                    }]
                }).then(alert => alert.present());
              } else {
                this.alertCtrl.create({
                  header: 'Alert!',
                  message: 'Invalid Scanned item. Please check and try again!',
                  buttons: [
                    {
                      text: 'Scan Again',
                      handler: () => {
                        this.scanQRCode();
                      }
                    },
                    {
                      text: 'Done',
                      handler: () => {
                        this.navCtrl.setDirection('forward');
                        this.router.navigateByUrl('/unsynced-sku');
                      }
                    }]
                }).then(alert => alert.present());
              }
            } else {

              setTimeout(() => {
                this.navCtrl.setDirection('forward');
                this.router.navigateByUrl('/unsynced-sku');
              }, 500);
            }
          }
          // if (!barcodeData.cancelled) {
          //   this.storeSKULocally(barcodeData.text, false);
          //   this.scanQRCode();
          // } else {
          //   setTimeout(() => {
          //     this.navCtrl.setDirection('forward');
          //     this.router.navigateByUrl('/unsynced-sku');
          //   }, 500);
          //   // console.log(localStorage.getItem('unsyncedSKU').split(','));
          // }
        }).catch(err => {
          console.log('Error', err);
        });
      }
    });
  }

  hitScannedURL(qrText) {

    const payload = {
      qr_codes: [{
        QR: qrText
      }]
    };
    console.log(payload);
    this.genPurpose.setLoaderStatus(true);
    this.apiService.getQRCodeDetails(payload).toPromise().then((resp: any) => {
      this.genPurpose.setLoaderStatus(false);
      console.log(resp);
      this.storeSKULocally(qrText, resp.data.qr_codes[0].product_name, false);
      this.alertCtrl.create({
        header: 'Your QR Code has been scanned successfully',
        message: resp.data.qr_codes[0].product_name.trim() == '' ? 'NO NAME FOUND' : `Product Name: ${resp.data.qr_codes[0].product_name}`,
        buttons: [
          {
            text: 'Scan Another',
            handler: () => {
              this.scanQRCode();
            }
          },
          {
            text: 'Done',
            handler: () => {
              this.navCtrl.setDirection('forward');
              this.router.navigateByUrl('/unsynced-sku');
            }
          }]
      }).then(alert => {
        alert.present();
      });
    },
      err => {
        this.storeSKULocally(qrText, '', false);
        this.genPurpose.setLoaderStatus(false);
        console.log(err);
        this.alertCtrl.create({
          header: 'Your QR Code has been scanned successfully',
          buttons: [
            {
              text: 'Scan Another',
              handler: () => {
                this.scanQRCode();
              }
            },
            {
              text: 'Done',
              handler: () => {
                console.log('Contents in LS', localStorage.getItem('unsyncedSKU'));
                this.navCtrl.setDirection('forward');
                this.router.navigateByUrl('/unsynced-sku');
              }
            }]
        }).then(alert => {
          alert.present();
        });
        // this.genPurpose.showToast(this.genPurpose.commonText.NO_INTERNET, 'top', 'dark');
      });
  }

  storeSKULocally(scannedSKU, qrName, isManual) {
    this.scannedText.push({
      qrCode: scannedSKU,
      qrName: qrName,
      scannedTime: moment().format('DD-MM-YYYY')
    });
    localStorage.setItem('unsyncedSKU', JSON.stringify(this.scannedText));
    // tslint:disable-next-line: max-line-length
    // if (this.scannedText.indexOf(scannedSKU) === -1 && scannedSKU.trim().length === 22 && scannedSKU.toString().match(Utility.MOBILE_REGX)) {
    //   this.scannedText.push(scannedSKU);
    //   localStorage.setItem('unsyncedSKU', this.scannedText.toString());
    // }
    // else {
    //   if (isManual) {
    //     this.genPurpose.showToast(this.genPurpose.commonText.INVALID_ITEM, 'top', 'dark');
    //   }
    // }
  }

  checkForSameEntry(scannedSKU) {
    let isPresent: boolean = false
    for (let item of this.scannedText) {
      if (item.qrCode === scannedSKU) {
        isPresent = true;
      }
    }
    return isPresent;
  }


  submitQRCode() {
    if (this.checkForSameEntry(this.qrCode) === false && this.qrCode.trim().length === 22 && this.qrCode.toString().match(Utility.MOBILE_REGX)) {
      this.hitScannedURL(this.qrCode);
    }
    else {
      this.genPurpose.showToast(this.genPurpose.commonText.INVALID_ITEM, 'top', 'dark');
    }
    // this.storeSKULocally(this.qrCode, true);
    // this.genPurpose.showToast(this.genPurpose.commonText.ITEM_ADDED_TO_UPLOADED_QUEUE, 'top', 'dark');
    this.qrCode = '';
  }

}
