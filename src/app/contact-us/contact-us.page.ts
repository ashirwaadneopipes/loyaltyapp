import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.page.html',
  styleUrls: ['./contact-us.page.scss'],
})

export class ContactUsPage implements OnInit {
  userObj: any;
  whatsAppMessage: any;
  tickets: any[] = [
    {
      "id": "TN123456789", "desc": "Pipes are Faulty ", "status": "In Process", "date": "16 Feb 2019"
    },
    {
      "id": "TN123456789", "desc": "Leakage in Pipe Fitting", "status": "Closed", "date": "26 Jan 2019"
    },
    {
      "id": "TN123456789", "desc": "Need Bigger Size spanners", "status": "Successful", "date": "13 Mar 2018"
    }
  ];

  constructor(private sessionService: SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    this.whatsAppMessage = "Hi, I am " + this.userObj.name + " I would like to get in touch with you.";
  }
}