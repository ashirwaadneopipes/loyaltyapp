import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-silver-benefits',
  templateUrl: './silver-benefits.page.html',
  styleUrls: ['./silver-benefits.page.scss'],
})
export class SilverBenefitsPage implements OnInit {

  userObj : any;
  constructor(private sessionService : SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
  }

}
