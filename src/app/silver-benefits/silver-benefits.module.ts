import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SilverBenefitsPage } from './silver-benefits.page';

const routes: Routes = [
  {
    path: '',
    component: SilverBenefitsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SilverBenefitsPage]
})
export class SilverBenefitsPageModule {}
