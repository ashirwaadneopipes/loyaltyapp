import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallsService } from '../services/api-calls.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import * as moment from 'moment';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-point-balance',
  templateUrl: './point-balance.page.html',
  styleUrls: ['./point-balance.page.scss'],
})
export class PointBalancePage implements OnInit {

  constructor(private router : Router,private genService : GeneralPurposeService, private apiService : ApiCallsService, private sessionService : SessionService) { }
  pointbalances: any[] = [];
  userObj : any;
  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
    this.genService.setLoaderStatus(true);
    this.apiService.getPointBalance().toPromise().then(
      (data : any) => {
       console.log(data);
       this.pointbalances = data.data;
      },
      (err : any)=>{

      }
    ).finally(()=>this.genService.setLoaderStatus(false));
  }

  dateFormat(date){
    return moment(date).format('Do MMM YYYY');
  }

}
