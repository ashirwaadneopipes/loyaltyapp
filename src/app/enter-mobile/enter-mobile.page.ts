import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Utility } from '../validators/utility';
import { ToastController } from '@ionic/angular';
import { ApiCallsService } from '../services/api-calls.service';
import { DatatransferService } from '../services/datatransfer.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import { SessionService } from '../services/session.service';



@Component({
  selector: 'app-enter-mobile',
  templateUrl: './enter-mobile.page.html',
  styleUrls: ['./enter-mobile.page.scss'],
})
export class EnterMobilePage implements OnInit {
  mobile_number: string = "";
  constructor(private router: Router, private apiService: ApiCallsService, private toastCtrl: ToastController,
    private datatransfer: DatatransferService, private genService: GeneralPurposeService, private sessionService: SessionService) { }

  ngOnInit() {
    // let userId = this.sessionService.getUserId();
    // console.log(userId);
    // if (userId) {
    //   this.genService.setLoaderStatus(true);
    //   this.apiService.dashboardGetData({'_id':userId}).toPromise().then(
    //     (data: any) => {
    //       console.log(data);
    //       if(data.success){
    //         this.sessionService.setUserDetails(data.data);
    //         this.router.navigate(['dashboard']);
    //       } else {

    //       }
    //       this.genService.setLoaderStatus(false);
    //     },
    //     (err: any) => {
    //       console.log(err);
    //       this.genService.setLoaderStatus(false);
    //     });
    // }
    
  }

  submitMobileNo() {
    if (!this.mobile_number.match(Utility.MOBILE_REGX)) {
      this.genService.showToast(this.genService.commonText.INVALID_MOBILE_NO, 'top', 'dark')
      return true;
    }
    const postData = {
      mobile_number: this.mobile_number
    };
    this.genService.setLoaderStatus(true);
    this.apiService.getOtp(postData).toPromise().then((data: any) => {
      this.genService.setLoaderStatus(false);
      if (data.success) {
        this.datatransfer.mobile_number = data.data.mobile_number;
        this.router.navigate(['/enter-otp']);
        this.genService.showToast(this.genService.commonText.OTP_SEND + data.data.mobile_number, 'top', 'dark');
      } else {
        this.genService.showToast(this.genService.commonText.OTP_SENDING_FAILED, 'top', 'dark');
      }
    },
      (err: any) => {
        this.genService.setLoaderStatus(false);
        this.genService.showToast(this.genService.commonText.SERVICE_ERROR, 'top', 'dark');
      });
  }

  mobileNumberEntered() {
    //console.log(this.mobileNo);
  }
}
