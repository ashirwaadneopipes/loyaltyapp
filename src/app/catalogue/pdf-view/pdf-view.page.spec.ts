import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfViewPage } from './pdf-view.page';

describe('PdfViewPage', () => {
  let component: PdfViewPage;
  let fixture: ComponentFixture<PdfViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
