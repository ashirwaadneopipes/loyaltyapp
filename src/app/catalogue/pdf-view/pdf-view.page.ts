import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { GeneralPurposeService } from 'src/app/services/general-purpose.service';

@Component({
  selector: 'app-pdf-view',
  templateUrl: './pdf-view.page.html',
  styleUrls: ['./pdf-view.page.scss'],
})
export class PdfViewPage implements OnInit {

  pdfUrl: any = null;
  showpdfUrl: any;
  showImg: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, private sanitizer: DomSanitizer, private genService: GeneralPurposeService) { }

  ngOnInit() {
    this.pdfUrl = this.route.snapshot.paramMap.get('id');
    if (this.pdfUrl == 'PDF 1.jpg') {
      this.pdfUrl = '../../../assets/imgs/' + this.pdfUrl;
      this.showImg = true;
    } else {
      setTimeout(() => {
        this.genService.setLoaderStatus(false);
      }, 5000);
      this.genService.setLoaderStatus(true);

      this.getPdfContent();
    }
    console.log(this.pdfUrl);
  }

  getPdfContent() {
    this.showpdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://docs.google.com/viewerng/viewer?url=' + this.pdfUrl + '&embedded=true');
  }

  iframeLoaded() {
    // this.genService.setLoaderStatus(false);
  }

}
