import { Component, OnInit } from '@angular/core';
import { environment } from  "../../environments/environment";
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.scss'],
})
export class CataloguePage implements OnInit {

  catalogueList = [
    {
      title : 'PROMASTOPÆ FIRE COLLARS',
      imageUrl : '../../assets/imgs/AVD 0215_Leaflet FC_v6.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0215_Leaflet FC_v6.pdf`,
    },
  {
      title : 'GREASE INTERCEPTOR',
      imageUrl : '../../assets/imgs/AVD 0215_Leaflet GI_v6.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0215_Leaflet GI_v6.pdf`,
    },
{
      title : 'SOLIDS INTERCEPTOR',
      imageUrl : '../../assets/imgs/AVD 0215_Leaflet SI_v7.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0215_Leaflet SI_v7.pdf`,
    },
{
      title : 'SINGLE STACK SWR',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet Aerator_v3_Aw1.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet Aerator_v3_Aw1.pdf`,
    },
{
      title : 'AGRI PIPES',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet Agri_v2.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet Agri_v2.pdf`,
    },
{
      title : 'uPVC AQUALIFE SYSTEM',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet Aqua_v6_Aw1.jpg',
      url : `PDF 1.jpg`,
      // url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet Aqua_v6_Aw1.pdf`,
    },
{
      title : 'SWR DRAINAGE SYSTEM',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet SWR_v7_Aw1.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet SWR_v7_Aw1.pdf`,
    },
{
      title : 'PAN CONNECTORS',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet WC_v7.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet WC_v7.pdf`,
    },
{
      title : 'FIRE PROTECTION SYSTEMS',
      imageUrl : '../../assets/imgs/AVD 0914_Leaflet_BlazeMaster_v2_Aw1.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0914_Leaflet_BlazeMaster_v2_Aw1.pdf`,
    },
{
      title : 'WATER HAMMER ARRESTOR',
      imageUrl : '../../assets/imgs/AVD 1014_Leaflet WHA_v3_Aw1.jpg',
      url : `${environment.pdfUploadedUrl}AVD 1014_Leaflet WHA_v3_Aw1.pdf`,
    },
    {
      title : 'LOW NOISE SWR SYSTEM',
      imageUrl : '../../assets/imgs/AVD 0115_Leaflet SSWR_v6.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0115_Leaflet SSWR_v6.pdf`,
    },
{
      title : 'uPVC UNDERGROUND DRAINAGE',
      imageUrl : '../../assets/imgs/AVD 0115_Leaflet UG_v6.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0115_Leaflet UG_v6.pdf`,
    },
{
      title : 'AIR ADMITTANCE VALVE',
      imageUrl : '../../assets/imgs/AVD 0215_Leaflet AAV_v6.jpg',
      url : `${environment.pdfUploadedUrl}AVD 0215_Leaflet AAV_v6.pdf`,
    },
  ];
  constructor(private router:Router) { }

  ngOnInit() {
  }

  openPDF(url){
    this.router.navigate(['/pdf-view', url]);
  }
}
