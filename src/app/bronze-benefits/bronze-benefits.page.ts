import { Component, OnInit } from '@angular/core';
import { SessionService } from '../services/session.service';

@Component({
  selector: 'app-bronze-benefits',
  templateUrl: './bronze-benefits.page.html',
  styleUrls: ['./bronze-benefits.page.scss'],
})
export class BronzeBenefitsPage implements OnInit {

  userObj : any;
  constructor(private sessionService : SessionService) { }

  ngOnInit() {
    this.userObj = this.sessionService.getUserDetails();
    console.log(this.userObj);
  }

}
