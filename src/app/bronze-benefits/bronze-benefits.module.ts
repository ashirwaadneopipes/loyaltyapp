import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BronzeBenefitsPage } from './bronze-benefits.page';

const routes: Routes = [
  {
    path: '',
    component: BronzeBenefitsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BronzeBenefitsPage]
})
export class BronzeBenefitsPageModule {}
