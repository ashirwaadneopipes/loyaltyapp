import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Utility } from '../validators/utility';
import { GeneralPurposeService } from '../services/general-purpose.service';
import { ApiCallsService } from '../services/api-calls.service';
import { SessionService } from '../services/session.service';
import { DatatransferService } from '../services/datatransfer.service';
import { IonDatetime, ActionSheetController, NavController } from '@ionic/angular';
import * as moment from 'moment';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild('dobPicker') dobPicker: IonDatetime;
  @ViewChild('domPicker') domPicker: IonDatetime;
  userType = '3';
  mobile_number: any;
  dateOFBirth = '';
  dateOFMarriage = '';
  defProfilePath: any = "assets/svgs/User.svg";
  profilePath: any = '';
  viewDocumentPath: any = '';
  documentPath: any = '';
  constructor(private genPurpose: GeneralPurposeService, private apiService: ApiCallsService,
    private sessionService: SessionService,
    private datatransfer: DatatransferService, private camera: Camera,
    private actionCtrl: ActionSheetController, private sanitizer: DomSanitizer,
    private navCtrl:NavController) { }
  ngOnInit() {
    console.log(this.datatransfer.mobile_number);
    if (this.datatransfer.mobile_number) {
      this.mobile_number = this.datatransfer.mobile_number;
      this.datatransfer.mobile_number = null;
    } else {
      this.navCtrl.navigateRoot('/enter-mobile');
      // this.router.navigateByUrl('/enter-mobile');
    }
  }

  registerFormDetails(form: NgForm) {
    console.log(form);
    if (!form.value.user_name.match(Utility.NAME_REGX)) {
      this.genPurpose.showToast('Please enter proper Name', 'top', 'dark');
      return;
    } else if (!form.value.email.match(Utility.EMAIL_REGX)) {
      this.genPurpose.showToast('Please enter proper Email Address', 'top', 'dark');
      return;
    } else if (!form.value.pincode.toString().match(Utility.MOBILE_REGX)) {
      this.genPurpose.showToast('Please enter proper Pin Code', 'top', 'dark');
      return;
    }

    //TODO need to add extra fields.

    this.genPurpose.setLoaderStatus(true);
    const payload = {
      name: form.value.user_name,
      mobile_number: this.mobile_number,
      email: form.value.email,
      user_type: 3, //1 = admin , 2 = dealer , 3 = plumber
      pin_code: form.value.pincode,
      profile_image: this.profilePath,
      doc_img: this.documentPath,
      address: `${form.value.address1} ${form.value.address2}`.trim(),
      dob: this.dateOFBirth == '' ? this.dateOFBirth : moment(this.dateOFBirth, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      wedding_date: this.dateOFMarriage == '' ? this.dateOFMarriage : moment(this.dateOFMarriage, 'DD-MM-YYYY').format('YYYY-MM-DD'),
    };
    console.log(payload);
    this.apiService.registerUser(payload).toPromise().then((resp: any) => {
      this.genPurpose.setLoaderStatus(false);
      if (resp.success) {
        this.sessionService.setUserDetails(resp.data.user_details);
        this.sessionService.setUserToken(resp.data.user_token);
        this.navCtrl.navigateRoot('/dashboard');
        // this.router.navigate(['/dashboard']);
      }
    },
      (err: any) => {
        this.genPurpose.setLoaderStatus(false);
        this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
      });
  }

  openDOBDatePicker() {
    this.dobPicker.open();
  }
  openDOMDatePicker() {
    this.domPicker.open();
  }

  dobChanged(dateEvent) {
    // console.log(moment(dateEvent.detail.value).format('DD-MM-YYYY'));
    this.dateOFBirth = moment(dateEvent.detail.value).format('DD-MM-YYYY');
  }

  domChanged(dateEvent) {
    // console.log(moment(dateEvent).format("DD-MM-YYYY"));
    this.dateOFMarriage = moment(dateEvent.detail.value).format('DD-MM-YYYY');
  }
  selectThisUserType(userType) {
    this.userType = userType;
  }

  askForMediaOptions(calledFrom) {
    this.actionCtrl.create({
      buttons: [{
        text: 'Camera',
        icon: 'ios-camera',
        handler: () => {
          const options: CameraOptions = {
            quality: 10,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.CAMERA,
            // allowEdit: true
          };
          this.getPicture(options, calledFrom);
        }
      }, {
        text: 'Gallery',
        icon: 'md-photos',
        handler: () => {
          const options: CameraOptions = {
            quality: 10,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
            // allowEdit: true
          };
          this.getPicture(options, calledFrom);
        }
      }, {
        text: 'Cancel',
        icon: 'md-close',
        role: 'cancel'
      }]
    }).then(actionSheet => {
      actionSheet.present();
    });
  }

  getPicture(options, calledFrom) {
    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      if (calledFrom === 'profile') {
        this.defProfilePath = this.sanitizeURL(imageData);
        this.profilePath = `data:Image/*;base64, ${imageData}`;
      } else {
        this.viewDocumentPath = this.sanitizeURL(imageData);
        this.documentPath = `data:Image/*;base64, ${imageData}`;
      }

    }, (err) => {
      console.log(err);
    });
  }
  sanitizeURL(imageData) {
    return this.sanitizer.bypassSecurityTrustUrl('data:Image/*;base64,' + imageData);
  }

}
