import { Component, OnInit } from '@angular/core';
import { ApiCallsService } from '../services/api-calls.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import { SessionService } from '../services/session.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.page.html',
  styleUrls: ['./portfolio.page.scss'],
})
export class PortfolioPage implements OnInit {
  imageList = [];
  category;
  userObj;
  imgUrlDomain = environment.baseurl + '/uploads/user/profile/';
  joinedDate: string;
  constructor(private apiService: ApiCallsService, private genPurpose: GeneralPurposeService,private sessionService : SessionService) {
    this.category = 'myWorkshop';
  }

  ngOnInit() {
    let userId = this.sessionService.getUserId();
    this.userObj = this.sessionService.getUserDetails();
    // console.log(userId);
    if (userId) {
      // this.genPurpose.setLoaderStatus(true);
      this.apiService.dashboardGetData({ '_id': userId }).toPromise().then(
        (data: any) => {
          if (data.success) {
            this.sessionService.setUserDetails(data.data);
            this.userObj = data.data;
            this.joinedDate = moment(this.userObj).format('Y');
          } else {

          }
          // this.genPurpose.setLoaderStatus(false);
        },
        (err: any) => {
          console.log(err);
          // this.genPurpose.setLoaderStatus(false);
        });
    }
  }

  ionViewWillEnter() {
    this.getImagesList();
  }

  getImagesList() {
    this.genPurpose.setLoaderStatus(true);
    this.apiService.getDummyImagesList().toPromise().then((resp: any) => {
      this.genPurpose.setLoaderStatus(false);
      this.imageList = resp;
    },
      (err: any) => {
        this.genPurpose.setLoaderStatus(false);
        this.genPurpose.showToast('Something Went Wrong', 'top', 'dark');
      }).finally(()=>{
        this.genPurpose.setLoaderStatus(false);
      });
  }

  comingSoon(){
    alert('Coming Soon');
  }

  formatDate(date) {
    return moment(date).format('DD-MMM-YYYY');
  }


}
