export class Utility {
  public static NAME_REGX = '^[a-zA-Z0-9_ ]*$';
  public static MOBILE_REGX = '^[0-9]*$';
  public static DATE_REGX = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
  public static URL_REGX = /^((https?):\/\/)?([w|W]{3}\.)+[a-zA-Z0-9\-\.]{3,}\.[a-zA-Z]{2,}(\.[a-zA-Z]{2,})?$/;
  public static EMAIL_REGX = /^([a-zA-Z0-9\._+]+)@([a-zA-z0-9-]+).([a-z]{2,8})(.[a-z]{2})?$/;;
  public static ONLY_ALPHA_REGX = /^[A-Za-z -]+$/;
  public static CK_EDITOR = /^[a-zA-Z0-9$@$!%*?&#^\-_.&;,  +<>/\s]+$/;
  public static SEARCH = /^[a-zA-Z0-9@ .,/-]*$/;
  public static ENG_NUM_SYM = /^[a-zA-Z0-9$@$!%*?&#^\-_.&;,  +<>/\s]+$/;
  public static EMOJI = /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/;
  public static COMP_NAME = /^[a-zA-Z0-9 .,/-]*$/;
  public static SEARCH_ALPHANUMERIC = /^[a-zA-Z0-9]*$/;
  public static SERVEYLINK_URL_REGEX = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/g;
}