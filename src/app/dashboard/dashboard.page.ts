import { Component } from '@angular/core';
import { ApiCallsService } from '../services/api-calls.service';
import { SessionService } from '../services/session.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage {
  imgUrlDomain = environment.baseurl + '/uploads/user/profile/';
  currentProgress = '300';
  userObj: any;
  maxMemberValue: any;
  joinedDate: string;
  backButtonSubscription: Subscription;
  constructor(private apiService: ApiCallsService, private navCtrl: NavController, private sessionService: SessionService, private genService: GeneralPurposeService,
    private alertCtrl: AlertController, private platform: Platform) { }

  ionViewWillEnter() {
    this.updateUserObj(this.sessionService.getUserDetails())
    this.getDashBoardData();
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
  }

  ionViewDidEnter() {
    localStorage.setItem('loggedIn', 'true');
  }

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  getDashBoardData() {
    let userId = this.sessionService.getUserId();
    console.log(userId);
    if (userId) {
      this.genService.setLoaderStatus(true);
      this.apiService.dashboardGetData({ '_id': userId }).toPromise().then(
        (data: any) => {
          console.log(data);
          if (data.success) {
            this.sessionService.setUserDetails(data.data);
          } else {
            this.showBasicAlert();
          }
          this.genService.setLoaderStatus(false);
        },
        (err: any) => {
          console.log(err);
          this.genService.setLoaderStatus(false);
        }).finally(() => {
          this.genService.setLoaderStatus(false);
        });
    } else {
      this.showBasicAlert();
    }
  }

  clearSessionAndLogOut() {
    this.sessionService.clearSession();
    // this.router.navigateByUrl('/enter-mobile');
    this.navCtrl.navigateRoot('/enter-mobile');
  }

  showBasicAlert() {
    this.alertCtrl.create({
      header: 'Alert',
      message: 'Some Error For Now. Redirecting you to Login Page',
      buttons: [{
        text: 'OK',
        handler: () => {
          this.clearSessionAndLogOut();
        }
      }]
    }).then(alert => alert.present());
  }

  updateUserObj(obj: any) {
    console.log(obj);
    this.userObj = obj;
    this.joinedDate = moment(obj.createdAt).format('Y');
  }

  nameParser(str) {

    return str != null ? (str.substr(0, str.indexOf(' ') > -1 ? str.indexOf(' ') : str.length)) : null;
  }

  menuGrid() {

  }

}
