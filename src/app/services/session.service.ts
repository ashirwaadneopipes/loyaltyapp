import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as Rx from 'rxjs';
import * as CryptoTS from 'crypto-ts';
import { DatatransferService } from './datatransfer.service';

const SECRET_KEY = '786Qwe1Poi0aSd2lKj8zxD3mnB5786';
@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private userObjPrivate = new Rx.BehaviorSubject({});
  userObjObservable = this.userObjPrivate.asObservable();
  loggedInStatus = new Rx.BehaviorSubject(false);
  isLoggedIn = this.loggedInStatus.asObservable();
  constructor(private datatransfer : DatatransferService) {
    this.loggedInStatus.next(false);
  }

  setUserDetails(info: any) {
    let userEncryptedString = CryptoTS.AES.encrypt(JSON.stringify(info), SECRET_KEY);
    localStorage.setItem('user', userEncryptedString.toString());
    this.userObjPrivate.next(info);
    this.datatransfer.userObject = info;
    this.setUserLoggedInStatus(true);
  }

  getUserDetails() {
    let tempUser = localStorage.getItem('user');
    let userDecryptedBytes, userDecryptedText;
    if (tempUser) {
      userDecryptedBytes = CryptoTS.AES.decrypt(tempUser, SECRET_KEY);
      userDecryptedText = userDecryptedBytes.toString(CryptoTS.enc.Utf8);
    }
    this.userObjPrivate.next(tempUser ? JSON.parse(userDecryptedText) : null);
    return tempUser ? JSON.parse(userDecryptedText) : null;
  }

  setUserToken(info: any) {
    let userEncryptedString = CryptoTS.AES.encrypt(JSON.stringify(info), SECRET_KEY);
    localStorage.setItem('user_token', userEncryptedString.toString());
    this.setUserLoggedInStatus(true);
  }

  getUserToken() {
    let tempUser = localStorage.getItem('user_token');
    let userDecryptedBytes, userDecryptedText;
    if (tempUser) {
      userDecryptedBytes = CryptoTS.AES.decrypt(tempUser, SECRET_KEY);
      userDecryptedText = userDecryptedBytes.toString(CryptoTS.enc.Utf8);
    }
    return tempUser ? JSON.parse(userDecryptedText) : null;
  }

  clearSession() {
    sessionStorage.clear();
    localStorage.clear();
    // for (const key in localStorage) {
    //   localStorage.removeItem(key);
    // }
    this.setUserLoggedInStatus(false);
  }

  setUserLoggedInStatus(flag) {
    this.loggedInStatus.next(flag);
  }

  getUserId() {
    const tempUser = localStorage.getItem('user');
    let userDecryptedBytes;
    let userDecryptedText;
    if (tempUser) {
      userDecryptedBytes = CryptoTS.AES.decrypt(tempUser, SECRET_KEY);
      userDecryptedText = userDecryptedBytes.toString(CryptoTS.enc.Utf8);
    }
    const userDetails = tempUser ? JSON.parse(userDecryptedText) : {};
    // console.log(userDetails);
    const userId = userDetails.hasOwnProperty('_id') ? userDetails._id : null;
    return userId;
  }

  getRoleId() {
    let tempUser = localStorage.getItem('user');
    let userDecryptedBytes;
    let userDecryptedText;
    if(tempUser) {
      userDecryptedBytes = CryptoTS.AES.decrypt(tempUser, SECRET_KEY);
      userDecryptedText = userDecryptedBytes.toString(CryptoTS.enc.Utf8);
    }
    const userDetails = tempUser ? JSON.parse(userDecryptedText) : {};
    const roleId = userDetails.hasOwnProperty('user') ? userDetails.user.roleId : null;
    console.log(roleId);
    return roleId;
  }
}

