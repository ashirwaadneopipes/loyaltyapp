import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UrlConstantService } from '../services/urlConstants.service';
import { BehaviorSubject, Observable, pipe, of, } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiCallsService {
  constructor(private httpClient: HttpClient, private constantService: UrlConstantService) {

  }

  getOtp(data) {
    return this.httpClient.post(this.constantService.sendOtp, data);
  }

  verifyOtp(data) {
    console.log(data);
    console.log('VerifyOtp');
    console.log(this.constantService.verifyOtp);
    return this.httpClient.post(this.constantService.verifyOtp, data);
  }

  registerUser(payload) {
    return this.httpClient.post(this.constantService.register, payload);
  }

  uploadSKU(payload) {
    return this.httpClient.post(this.constantService.uploadSKU, payload);
  }

  dashboardGetData(payload) {
    return this.httpClient.post(this.constantService.dashboardGet, payload);
  }

  getQRCodeDetails(payload) {
    return this.httpClient.post(this.constantService.getQRDetails, payload);
  }

  buyNow(payload) {
    return this.httpClient.post(this.constantService.buynow, payload);
  }

  getDummyImagesList() {
    return this.httpClient.get('https://picsum.photos/v2/list');
  }

  orderList() {
    return this.httpClient.get(this.constantService.orderList);
  }

  getPointBalance() {
    return this.httpClient.get(this.constantService.pointBalance);
  }

  postNewQuestion(payload) {
    return this.httpClient.post(this.constantService.communityQuestions, payload);
  }

  listCommunityQuestions() {
    return this.httpClient.get(this.constantService.communityQuestions);
  }

  fetchQuestionDetails(payload){
    return this.httpClient.post(this.constantService.communityQuestionDetail , payload);
  }

  postComment(payload){
    return this.httpClient.post(this.constantService.postComment , payload);
  }


}
