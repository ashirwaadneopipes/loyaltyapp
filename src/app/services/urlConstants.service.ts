import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UrlConstantService {

  api = '/api';

  //-------------- OTP ---------------//
  sendOtp = environment.baseurl + this.api + '/user/send-otp';
  verifyOtp = environment.baseurl + this.api + '/user/verify-otp';

  // ----------------Register User -------------//
  register = environment.baseurl + this.api + '/user/registration';

  uploadSKU = environment.baseurl + this.api + '/qr-code/is-qr-valid';

  dashboardGet = environment.baseurl + this.api + '/user/get-by-id';
  getQRDetails = environment.baseurl + this.api + '/qr-code/get-product-type-name';


  // --------- Redeem ----------
  buynow = environment.baseurl + this.api + '/redeem/add-redeem';


  // ---------- Order List -----------//
  orderList = environment.baseurl + this.api + '/redeem/get-by-user';


  // ----------- Point Balance ---------- //
  pointBalance = environment.baseurl + this.api + '/transaction/get-by-user';


  // -----------  Community ---------- //
  communityQuestions = environment.baseurl + this.api + '/question';


  // -----------  Community getQuestion Detail---------- //
  communityQuestionDetail = environment.baseurl + this.api + '/question/get-by-id';

  // -----------  Community Post a comment---------- //
  postComment = environment.baseurl + this.api + '/comment';

  constructor() { }

}
