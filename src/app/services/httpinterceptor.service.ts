import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from './session.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    constructor(private router: Router, private sessionService: SessionService) { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        const user_token = this.sessionService.getUserToken();
        const requestClone = request.clone({
            headers: user_token ? request.headers.set('Authorization', user_token) : null
        });

        return next.handle(requestClone).pipe(
            tap(
                event => {
                    if (event instanceof HttpResponse) {
                        // console.log('api call success :', event);
                    }
                },
                error => {
                    // console.log('api call error',error);
                    if (error instanceof HttpErrorResponse) {
                        if (error.status === 500 || error.status === 503) {
                            // console.log("api call error :", error);
                            alert('Error Page pending');
                            // this.router.navigate(['/back-soon-page']);
                        }
                    }
                }
            )
        );
    }
}
