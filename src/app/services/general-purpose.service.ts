import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GeneralPurposeService {

  loader: any;
  showLoader: boolean = false;
  constructor(private toastCtrl: ToastController, private loadingCtrl: LoadingController) {
  }

  commonText = {
    ITEM_ADDED_TO_UPLOADED_QUEUE: 'Item Added.',
    INVALID_ITEM: 'Invalid Entry',
    SERVICE_ERROR: 'Something Went Wrong',
    INVALID_MOBILE_NO: 'Please enter proper Mobile Number',
    OTP_SEND: 'OTP send to mobile number ',
    OTP_SENDING_FAILED: 'Failed to send OTP',
    NO_INTERNET: 'No Internet Detected'
  };

  showToast(message, position, color, duration = 3000) {
    this.toastCtrl.create({
      message,
      duration,
      position,
      color,
    }).then((toastData) => {
      toastData.present();
    });
  }

  setLoaderStatus(flag = true) {
    if (flag) {
      this.showLoader = true;
      this.loader = null;
      this.loadingCtrl.create(
        {
          message: 'Loading...'
        }
        //   {
        //   spinner: null,
        //   message: `
        //   <div class="custom-spinner-container">
        //   <img class="loading" width="120px" height="120px" src="assets/imgs/loading.gif" />
        // </div>`
        // }
      ).then(loader => {
        this.loader = loader;
        loader.present().then(() => {
          if (!this.showLoader) {
            loader.dismiss();
          }
        });
      });
    }
    else {
      this.showLoader = false;
      this.loadingCtrl.dismiss();
    }
  }

}

