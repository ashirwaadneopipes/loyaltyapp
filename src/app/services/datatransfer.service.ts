import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatatransferService {


  private mobile_numberVar : any=null;
  private userObjectVar : any = null;

  constructor() { }


  set mobile_number(value) {
    this.mobile_numberVar = value;
  }

  get mobile_number() {
    return this.mobile_numberVar;
  }

  set userObject(value){
    this.userObjectVar = value;
  }

  get userObject(){
    return this.userObjectVar;
  }


  scannedServiceObj:any;

}

