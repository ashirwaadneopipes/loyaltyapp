import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCallsService } from '../services/api-calls.service';
import { GeneralPurposeService } from '../services/general-purpose.service';
import * as moment from 'moment';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.page.html',
  styleUrls: ['./order-status.page.scss'],
})
export class OrderStatusPage implements OnInit {
  orderlists: any[] = [
    // {
    //   "name": "HDFC Gift Voucher","id" : "123432","date":"2015-05-31","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // },
    // {
    //   "name": "Amazon Gift Voucher","id" : "234323","date":"2015-05-12","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // },
    // {
    //   "name": "Flipkart Gift Voucher","id" : "123543","date":"2015-04-15","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // },
    // {
    //   "name": "Zomato Gift Voucher","id" : "123234","date":"2015-05-31","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // },
    // {
    //   "name": "HDFC Gift Voucher","id" : "125643","date":"2015-04-13","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // },
    // {
    //   "name": "Amazon Gift Voucher","id" : "126543","date":"2015-06-07","thumbnail":"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
    // }
  ];

  constructor(private router: Router,private apiService : ApiCallsService, private genService : GeneralPurposeService) { }

  ngOnInit() {
    // this.router
    this.genService.setLoaderStatus(true);
    this.apiService.orderList().toPromise().then((data:any)=>{
      console.log(data);
      this.orderlists = data.data;
    },(err:any)=>{

    }).finally(()=>this.genService.setLoaderStatus(false));
  }

  dateFormat(date){
    return moment(date).format('DD-MM-Y');
  }

}
