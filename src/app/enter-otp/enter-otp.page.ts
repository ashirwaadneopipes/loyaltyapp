import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Utility } from '../validators/utility';
import { DatatransferService } from '../services/datatransfer.service';
import { ToastController, NavController } from '@ionic/angular';
import { ApiCallsService } from '../services/api-calls.service';
import { SessionService } from '../services/session.service';
import { GeneralPurposeService } from '../services/general-purpose.service';

@Component({
  selector: 'app-enter-otp',
  templateUrl: './enter-otp.page.html',
  styleUrls: ['./enter-otp.page.scss'],
})
export class EnterOTPPage {
  otp = ['', '', '', ''];
  allowedDigits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
  otpString: any;
  mobile_number: any;
  constructor(private router: Router, private datatransfer: DatatransferService,
    private apiService: ApiCallsService,
    private toastCtrl: ToastController,
    private sessionService: SessionService,
    private genService: GeneralPurposeService,
    private navCtrl: NavController) {

  }

  ionViewWillEnter() {
    console.log(this.datatransfer.mobile_number);
    if (this.datatransfer.mobile_number) {
      this.mobile_number = this.datatransfer.mobile_number;
      this.datatransfer.mobile_number = null;
    } else {
      this.router.navigateByUrl('/enter-mobile');
    }
  }

  moveFocus(events, currentindex, nextElement?, prevElement?) {
    // console.log(events);
    if (this.allowedDigits.indexOf(events.key) > -1) {
      this.otp[currentindex] = events.key;
      if (nextElement && this.otp[currentindex] !== '') {
        this.otp[currentindex + 1] = '';
        nextElement.setFocus();
      }
    }
    else if (events.key === 'Backspace') {

      if (prevElement && this.otp[currentindex] == '') {
        prevElement.setFocus();
      }
      else if (prevElement && this.otp[currentindex] != '') {
        this.otp[currentindex] = '';
        // prevElement.setFocus();
      }
    }
    else {
      this.otp[currentindex] = '';
    }
    console.log(this.otp);
  }

  getLastItem(events, currentindex, prevElement?) {
    if (this.allowedDigits.indexOf(events.key) > -1) {
      this.otp[currentindex] = events.key;
    } else if (events.key === 'Backspace') {
      if (prevElement && this.otp[currentindex] == '') {
        prevElement.setFocus();
      }
      else if (prevElement && this.otp[currentindex] != '') {
        this.otp[currentindex] = '';
        // prevElement.setFocus();
      }
    } else {
      this.otp[currentindex] = '';
    }
    console.log(this.otp);
  }
  resendOTP() {

  }

  submitOTP() {
    this.otpString = this.otp.join('');
    console.log(this.otpString);
    if (!this.otpString.match(Utility.MOBILE_REGX)) {
      this.genService.showToast('Enter Numbers in OTP', 'top', 'dark');
    }
    else {
      const postData = {
        mobile_number: this.mobile_number,
        otp_to_verify: this.otpString
      };
      this.genService.setLoaderStatus(true);

      this.apiService.verifyOtp(postData).toPromise().then((data: any) => {
        console.log(data);
        if (data.success) {
          this.datatransfer.mobile_number = data.data.mobile_number;
          if (data.data.is_exist) {
            this.sessionService.setUserDetails(data.data.user_details);
            this.sessionService.setUserToken(data.data.user_token);
          }
          this.genService.showToast(' OTP verified, ' + (data.data.is_exist ? ' Logged in Successfully' : ' Registration Required '), 'top', 'success');
          this.navCtrl.setDirection('root');
          this.router.navigate([data.data.is_exist ? '/dashboard' : '/register']);

        } else {
          this.genService.showToast(data.message, 'top', 'dark');
        }
      },
        (err: any) => {
          console.log(err);
          this.genService.showToast('Something Went Wrong', 'top', 'dark');
        }).finally(() => {
          this.genService.setLoaderStatus(false);
        });
    }
  }

}
